echo off
title DIFF
echo ignore all the warnings this code produces
set /P id=Press ENTER to BEGIN:


set /P idurl=Enter https of the git repo:
rem https://gitlab.com/sanjay98/test_branch.git
git clone %idurl%

set /P idfolder=Enter Folder Name:
rem test_branch
cd %idfolder%

set /P id1=Enter branch name 1:
rem master
git branch %id1% origin/%id1%

set /P id2=Enter branch name 2:
rem test
git branch %id2% origin/%id2%


set /P id3=enter file name:
rem test.txt

set /P id4=enter file name where you want to save diff:
rem diff.txt

echo do you want to view it visually?? Y or N
git difftool %id1% %id2% %id3% 

echo saving file....
git diff -plain %id1% %id2% %id3% >  ../%id4%
git diff --stat %id1% %id2% %id3% 
cd ..
rem rmdir %idfolder% /s /q
echo END 
